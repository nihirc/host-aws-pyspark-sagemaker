FROM ubuntu:16.04

RUN apt-get -y update && apt-get install -y --no-install-recommends \
         wget \
         python3 \
         nginx \
         ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py && \
    pip install numpy==1.16.2 pandas boto3 awscli flask gevent gunicorn pyspark==2.4.4 && \
        (ln /usr/local/lib/python3.5/dist-packages/numpy/.libs/* .) && \
        rm -rf /root/.cache

ENV PYTHONUNBUFFERED=TRUE
ENV PYTHONDONTWRITEBYTECODE=TRUE
ENV PATH="/opt/program:${PATH}"

RUN apt-get -y update && apt-get install -y openjdk-8-jdk openjdk-8-jre

COPY titanic /opt/ml/model/
# foresight.Jar, Model , foresight python packages 
# Karthi changes

# RUN apt-get update
# RUN apt-get install zip unzip

# ENV SPARK_HOME="/usr/local/lib/python3.5/dist-packages/pyspark"

# COPY foresight-poprec-assembly-0.5.0-SNAPSHOT-dist.zip /tmp/
# RUN cd /tmp && \
#      unzip foresight-poprec-assembly-0.5.0-SNAPSHOT-dist.zip && \
#      mv /tmp/foresight-poprec-assembly-0.5.0-SNAPSHOT/python/foresight-python-0.5.0-SNAPSHOT.zip $SPARK_HOME/python/ && \
#      mv /tmp/foresight-poprec-assembly-0.5.0-SNAPSHOT/python/foresight-bunsen-0.5.0-SNAPSHOT.zip $SPARK_HOME/python/ && \
#      mv /tmp/foresight-poprec-assembly-0.5.0-SNAPSHOT/lib/foresight-poprec-shaded-0.5.0-SNAPSHOT.jar $SPARK_HOME/jars/ && \
#      mv /tmp/foresight-poprec-assembly-0.5.0-SNAPSHOT/ml-models/pediatricAsthma_readmission_90days_cernerdemo/version_3/pediatricAsthma_readmission_90days_cernerdemo /opt/ml/ && \
#      rm -r foresight-poprec-assembly-0.5.0-SNAPSHOT-dist.zip && \
#      rm -r foresight-poprec-assembly-0.5.0-SNAPSHOT

# RUN cd $SPARK_HOME/python && \
#      unzip foresight-bunsen-0.5.0-SNAPSHOT.zip && \
#      unzip foresight-python-0.5.0-SNAPSHOT.zip && \
#      cd /tmp
## Karthi changes end


COPY predictor.py /opt/program/
COPY serve /opt/program/
COPY nginx.conf /opt/program/
COPY wsgi.py /opt/program/
COPY aws-java-sdk-1.7.3.jar $SPARK_HOME/jars/
COPY hadoop-aws-2.7.3.jar $SPARK_HOME/jars/
COPY jets3t-0.9.4.jar $SPARK_HOME/jars/
RUN chmod +x /opt/program/serve
ENV PYSPARK_DRIVER_PYTHON=python3
ENV PYSPARK_PYTHON=python3
WORKDIR /opt/program

