from __future__ import print_function

import os
import json
import pickle
from io import StringIO
import sys
import signal
import traceback
from boto3 import Session
import flask
import pyspark
import pandas as pd
from pyspark.sql import SparkSession
from pyspark.ml.classification import RandomForestClassificationModel
from pyspark.ml.feature import VectorAssembler


prefix = '/opt/ml/'
model_path = os.path.join(prefix, 'model')

# A singleton for holding the model. This simply loads the model and holds it.
# It has a predict function that does a prediction based on the model and the input data.

class ModelService(object):
    model = None
    spark = None

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.spark == None:
            credentials = cls.get_temporary_credentials()
            print(credentials.access_key)
            print(credentials.secret_key)
            print(credentials.token)
            os.environ["PYSPARK_SUBMIT_ARGS"] = ('--packages "org.apache.hadoop:hadoop-aws:2.7.3" pyspark-shell')
            cls.spark = SparkSession.builder.getOrCreate()
            hadoop_conf = cls.spark.sparkContext._jsc.hadoopConfiguration()
            hadoop_conf.set("fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
            sc = cls.spark.sparkContext
            sc.setSystemProperty("com.amazonaws.services.s3.enableV4", "true")
            hadoop_conf.set("com.amazonaws.services.s3.enableV4", "true")
            hadoop_conf.set("fs.s3a.endpoint", "s3.us-west-2.amazonaws.com")    
            hadoop_conf.set("fs.s3a.aws.credentials.provider","org.apache.hadoop.fs.s3a.BasicAWSCredentialsProvider")
            hadoop_conf.set("fs.s3a.access.key", credentials.access_key)
            hadoop_conf.set("fs.s3a.secret.key", credentials.secret_key)
            hadoop_conf.set("fs.s3a.session.token", credentials.token)
        
        if cls.model == None:
            cls.model = RandomForestClassificationModel.load(model_path)
        return cls.model

    @classmethod
    def predict(cls, input):
        """For the input, do the predictions and return them.
        Args:
            input (a pandas dataframe): The data on which to do the predictions. There will be
                one prediction per row in the dataframe"""
        clf = cls.get_model()
        required_features = ['Pclass','Age','Fare','Gender','Boarded']
        assembler = VectorAssembler(inputCols=required_features, outputCol='features')
        df = cls.spark.read.csv("s3://sagemaker-us-west-2-782067938675/data/abalone_train.csv")
        print(df.show(5))
        input_df = cls.spark.createDataFrame(input)
        transformed_data = assembler.transform(input_df)
        predictions = clf.transform(transformed_data).collect()
        return predictions
    
    @classmethod
    def get_temporary_credentials(cls):
        session = Session()
        print(session.profile_name)
        credentials = session.get_credentials()
        current_credentials = credentials.get_frozen_credentials()
        return current_credentials

# The flask app for serving predictions
app = flask.Flask(__name__)

@app.route('/ping', methods=['GET'])
def ping():
    """Determine if the container is working and healthy. In this sample container, we declare
    it healthy if we can load the model successfully."""
    health = ModelService.get_model() is not None  # You can insert a health check here

    status = 200 if health else 404
    return flask.Response(response='Ping Successful\n', status=status, mimetype='application/json')

@app.route('/invocations', methods=['POST'])
def transformation():
    """Do an inference on a single batch of data. In this sample server, we take data as CSV, convert
    it to a pandas data frame for internal use and then convert the predictions back to CSV (which really
    just means one prediction per line, since there's a single column.
    """
    input_sample = pd.DataFrame(data={
        "Survived": [0.0],
        "Pclass": [1.0],
        "Age": [21.0],
        "Fare": [77.2874],
        "Gender": [0.0],
        "Boarded": [0.0]
    })

    # Convert from CSV to pandas
#     if flask.request.content_type == 'text/csv':
#         data = flask.request.data.decode('utf-8')
#         s = StringIO.StringIO(data)
#         data = pd.read_csv(s, header=None)
#     else:
#         return flask.Response(response='This predictor only supports CSV data', status=415, mimetype='text/plain')

#     print('Invoked with {} records'.format(input_sample.show(1)))

    # Do the prediction
    predictions = ModelService.predict(input_sample)
    print(predictions)

    # Convert from numpy back to CSV
    out = StringIO()
    pd.DataFrame({'results':predictions}).to_csv(out, header=False, index=False)
    result = out.getvalue()

    return flask.Response(response=result, status=200, mimetype='text/csv')
